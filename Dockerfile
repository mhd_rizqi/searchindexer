#
# Build stage
#
FROM maven:3.8.1-openjdk-11-slim AS build
COPY searchindexer/src/ /home/app/src
COPY searchindexer/pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

#
# Package stage
#
FROM openjdk:11-jre-slim
VOLUME /tmp
ARG JAVA_OPTS
ENV JAVA_OPTS=$JAVA_OPTS
COPY searchindexer/target/searchindexer-0.0.1-SNAPSHOT.jar searchindexer.jar
EXPOSE 8080
# ENTRYPOINT exec java $JAVA_OPTS -jar searchindexer.jar
# For Spring-Boot project, use the entrypoint below to reduce Tomcat startup time.
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar searchindexer.jar
