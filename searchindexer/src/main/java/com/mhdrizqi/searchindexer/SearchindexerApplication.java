package com.mhdrizqi.searchindexer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SearchindexerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SearchindexerApplication.class, args);
	}

}
