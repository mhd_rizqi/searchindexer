package com.mhdrizqi.searchindexer.repositories;

import java.util.Properties;

import org.apache.kafka.clients.producer.ProducerConfig;

public class SearchIndexingKafkaRepository implements SearchIndexingRepository {
  public SearchIndexingKafkaRepository() {
    Properties kafkaConfig = new Properties();
    kafkaConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
    kafkaConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
    kafkaConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
  }
}
