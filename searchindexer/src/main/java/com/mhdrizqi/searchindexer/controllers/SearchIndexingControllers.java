package com.mhdrizqi.searchindexer.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchIndexingControllers {
  @GetMapping("/random-data-inputter")
  public String insertRandomData() {
    return "Hello world and dunya!";
  }
}
